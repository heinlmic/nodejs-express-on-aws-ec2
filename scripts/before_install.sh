#!/bin/#!/usr/bin/env bash
#donwload node and npm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.#!/bin/sh
nvm install node

#create our working directory if it doesnt existing
DIR="/home/ec2-user/express-app"
if [ -d "DIR" ]; then
  echo "${DIR} exists"
else
  echo "Creating ${DIR} directory"
  mkdir ${DIR}
fi
